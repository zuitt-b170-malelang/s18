/* Activity:
1. In the S18 folder, create an activity folder and an index.html and script.js file inside of it.
2. Link the script.js file to the index.html file.
3. Create a trainer object using object literals.
4. Initialize/add the following trainer object properties:
- Name (String)
- Age (Number)
- Pokemon (Array)
- Friends (Object with Array values for properties)
5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
6. Access the trainer object properties using dot and square bracket notation.
7. Invoke/call the trainer talk object method.
8. Create a constructor for creating a pokemon with the following properties:
- Name (Provided as an argument to the contructor)
- Level (Provided as an argument to the contructor)
- Health (Create an equation that uses the level property)
- Attack (Create an equation that uses the level property)
9. Create/instantiate several pokemon object from the constructor with varying name and level properties.
10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
11. Create a faint method that will print out a message of targetPokemon has fainted.
12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
13. Invoke the tackle method of one pokemon object to see if it works as intended.
14. Create a git repository named S18.
15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
16. Add the link in Boodle. */

let trainer = {
  name: "Ash",
  age: "10",
  pokemon: ["Pikachu", "Butterfree", "Pidgeot", "Charizard"],
  talk: function (){
    console.log(this.pokemon[0] + " ! I choose you!")
  },
  friends: {
    hoenn: ["May", "Max"],
    kanto: ["Brock", "Misty"]
  }
}
console.log("Result of Dot Notation:")
console.log(trainer.name);
console.log("Result of Square Bracket Notation:")
console.log(trainer['pokemon']);
console.log("Result of talk method:")
trainer.talk();

function Pokemon (name, level){
  this.name=name,
  this.level=level,
  this.health=2*level,
  this.attack=level+10,
  // tackle
  this.tackle = function(target){
      console.log(this.name + " tackled " + target.name)
      let life = (target.health - this.attack)

      if(life <= 0){
        this.faint()
      }
      else if(life>0){
        console.log(target.name + "'s health is now reduced to " + life)
      }
  },
  this.faint = function(){
    console.log("The targetPokemon has fainted.")
}
}

let Raichu = new Pokemon ("Raichu", 10);
let Blastoise = new Pokemon("Blastoise", 15);
let Magikarp = new Pokemon("Magikarp", 50);
let Pikachu = new Pokemon("Pikachu", 30);
let Charizard = new Pokemon("Charizard", 30);


Pikachu.tackle(Blastoise);
Pikachu.tackle(Magikarp);
Magikarp.tackle(Pikachu);
Magikarp.tackle(Raichu);
Magikarp.tackle(Charizard);
