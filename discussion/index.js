/* 
log in the console the following gwa per subject of a student:
98.5
94.3
89.2
90.0
*/

let grades = [98.5, 94.3, 89.2, 90.0]
console.log(grades);

/* 
Objects
  collection of related data and functionalities; usually representing real-world objects
*/
let grade = {
  /* object initializer - curly braces ({})
  
    value - information that serves as the value of the key/field
  */
  math: 98.5,
  english: 94.3,
  science: 90.0,
  MAPEH: 89.2
};
console.log(grade);
/* 
ACCESSING ELEMENTS INSIDE THE OBJECT
  SYNTAX FOR CREATING AN OBJECT:
    let ObjectName = {
      keyA: valueA,
      keyA: valueB
    }

  Dot Notation - used to access the specific propert and value of the object; preferred in terms of object access
*/

console.log(grade.english);
// accessing two keys in the object
console.log(grade.english, grade.math)

/* 
create a cellphone that has the following keys (supply your own values for each)
brand
color
mfd
*/

let phone = {
  brand: "samsung",
  color: "blue",
  mfd: 2022
};
console.log(phone);
// typeOf
console.log(typeof phone);

let student = {
  firstName: "John",
  lastName: "Smith",
  mobileNo: 09123456789,
  location: {
    city: "Tokyo",
    country: "Japan"
  },
  email: ["john@mail.com", "johm.smith@mail.com"],

  fullName: (function name(fullName){
    // this - refers to the object where it is inserted

    /* 
    in this case, the code below could be typed as console.log(student.firstName + " " + studen.lastName)
    */
    console.log(this.firstName + " " + this.lastName)
  })
};
student.fullName();

console.log(student.location.city);
console.log(student.email[1])

/* 
ARRAY OF OBJECTS
*/

let contactList = [
  {
    firstName: "John",
    lastName: "Smith",
    location: "Japan"
  },
  {
    firstName: "Jane",
    lastName: "Smith",
    location: "Japan"
  },
  {
    firstName: "Jasmine",
    lastName: "Smith",
    location: "Japan"
  }
];
// accessing an element inside the array
console.log(contactList[1])
// accessing a key in an object that is inside the array
console.log(contactList[2].firstName)

/* 
Constructor Function - create a reuseable function to create several objects
        - useful for creating copies/instances of an object

  SYNTAX:
  function objectName (keyA, keyB){
    this.keyA = keyA,
    this.keyB = keyB
  }
*/
/* 
object literals - let objectName = {}

instance - concrete occurence of any object which emphasizes on the unique identity of it

*/

// Reusable Function/Object Method
function Laptop (name, manufacturedDate){
  this.name=name,
  this.manufacturedDate = manufacturedDate
};

// new keyword refers/sognifies that there will be a new object under the Laptop function
let laptop1 = new Laptop ("Lenovo", 2008);
console.log(laptop1);
let laptop2 = new Laptop ("Toshiba", 1997);
console.log(laptop2);

/* Miniactivity
  create a variable and store the laptop1 and laptop2 in it
*/
let laptop = [laptop1, laptop2];
console.log(laptop)


/*
initializing, adding, deleting, reassigning of object properties
*/

let car = {};
console.log(car);

// adding object properties

car.name = "Honda Civic";
console.log(car);

car["manufacturedDate"] = 2020;
console.log(car);

// reassigning object (same/existing properties, but assigning a different value)
// car["name"] = "Volvo";
car.name = "Volvo";
console.log(car);


// deleting object properties


// delete car.manufacturedDate;
// console.log(car);

delete car['manufacturedDate'];
console.log(car);

let person = {
	name: "John",
	talk: function(){
		console.log('Hi! My name is ' + this.name);
	}
}
// add property
person.walk = function(){
	console.log(this.name + " walked 25 steps forward.");
}

let friend = {
  firstName: "Joe",
  lastName: "Doe",
  address:{
    city: "Austin",
    state: "Texas"
  },
  emails: ["joe@mail.com", "joedoe@mail.com"],
  introduce: function(){
    console.log("Hello! My name is " + this.firstName + " " + this.lastName)
  }
};

let pokemon = {
  name: "Pikachu",
  level: 3,
  health: 100,
  attack: 50,
  tackle: function(){
    console.log("This pokemon tackled targetPokemon")
    console.log("targetPokemon's heaelth is now reduced to targetPokemonHealth")
  }
};

function Pokemon (name, level){
  this.name=name,
  this.level=level,
  this.health=2*level,
  this.attack=level

// Skills of Pokemon
this.tackle = function(target){
  console.log(this.name + " tackled " + target.name)
  console.log("targetPokemon's health is now reduced to targetPokemonHealth")

},
this.faint = function(){
  console.log(this.name + " fainted.")
}
}

let Pikachu = new Pokemon ("Pikachu", 16);
let Charizard = new Pokemon("Charizard", 8);